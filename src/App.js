
import 'bootstrap/dist/css/bootstrap.min.css'
//import FC from './components/Color';
// import FavouriteColor, { color } from './components/Color';
//import { color, NewColor as NC, changeColor as CC } from './components/Color';
import * as MyColor from './components/Color';
import Input from './components/Input';
import NavMenu from "./components/NavMenu";
import MyCard from "./components/MyCard";
import MyTable from './components/MyTable';
import Toggle from './components/Toggle';
import Counter from './components/Counter';
import MyForm from './components/MyForm';
import MyUpdatedForm from './components/MyUpdatedForm';

function App() {
  let p = [
      {
        id: 1,
        name: "Bill",
        quote:
          "Some quick example text to build on the card title and make up the bulk of the card's content",
        image:
          "https://www.diethelmtravel.com/wp-content/uploads/2016/04/bill-gates-wealthiest-person.jpg",
      },
      {
        id: 2,
        name: "Dara",
        quote:
          "Some quick example text to build on the card title and make up the bulk of the card's content",
        image:
          "https://img.freepik.com/free-photo/handsome-young-businessman-shirt-eyeglasses_85574-6228.jpg?size=626&ext=jpg",
      },
      {
        id: 3,
        name: "Nita",
        quote:
          "Some quick example text to build on the card title and make up the bulk of the card's content",
        image:
          "https://img.freepik.com/free-photo/cheerful-curly-business-girl-wearing-glasses_176420-206.jpg?size=626&ext=jpg",
      },
      {
        id: 4,
        name: "Jiva",
        quote:
          "Some quick example text to build on the card title and make up the bulk of the card's content",
        image:
          "https://www.indiewire.com/wp-content/uploads/2015/04/dakota-fanning-by-daniel-bergeron.jpg?w=780",
      },
      {
        id: 5,
        name: "Bora",
        quote:
          "Some quick example text to build on the card title and make up the bulk of the card's content",
        image:
          "https://www.glassdoor.co.uk/blog/app/uploads/sites/3/why-are-you-the-best-person-for-this-job-724x450-1.jpeg",
      },
    ]

    console.log("Old data:",p);

  return (
    <div className="App">
      <NavMenu/>
      <MyCard person={p}/>
      <MyTable data={p}/>
      <Toggle/>
      <Counter/>

      {/* <MyForm/> */}
      {/* <MyUpdatedForm/> */}
      {/* <MyColor.default username="nita"/>
      <h1>{MyColor.color}</h1>
      <h1><MyColor.NewColor test="ABC" /></h1>
      <h1>{MyColor.changeColor()}</h1> */}
      {/* <FC/>
      <h1>{color}</h1>
      <h1>{NC()}</h1>
      <h1>{CC()}</h1> */}
      {/* <Input/> */}
    </div>
  );
}

export default App;


import React, { Component } from 'react'
import {Button} from 'react-bootstrap'

export default class Counter extends Component {
    constructor(props){
        super(props)

        this.state = {
            count: 0
        }
        //this.increase = this.increase.bind(this)
    }

    increase(e, param){
        //alert(param)
        this.setState(current=>({
            count: current.count + 1
        }))
    }

    decrease = (parm, e) => {
        //alert(param)
        this.setState({
            count: this.state.count - 1
        })
    }


    render() {
        return (
            <div className="w-25 mx-lg-auto my-lg-5 text-lg-center">
                <h1 style={{color: "green", fontSize: "100px"}}>{this.state.count}</h1>
                {/* <Button onClick={this.increase.bind(this)} className="w-25 mx-lg-2" variant="primary">+</Button> */}
                <Button name="IncreaseBTN" onClick={(e)=>this.increase(e, this.state.count)} className="w-25 mx-lg-2" variant="primary">+</Button>
                {/* <Button name="DecreaseBTN" onClick={this.decrease} className="w-25" variant="warning">-</Button> */}
                <Button name="DecreaseBTN" onClick={this.decrease.bind(this,this.state.count)} className="w-25" variant="warning">-</Button>
            </div>
        )
    }
}

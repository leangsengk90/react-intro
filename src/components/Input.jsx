
import React from 'react'

export default function Input() {

    function pressBtn(){
        console.log("Pressing...");
        //let arr = [10,20,30]
        // const [a,b,c] = arr
        // console.log("a",a);
    }

    //console.log("PRESS: "+pressBtn());

    return (
        <div>
            <input type="text" />
            <button onClick={pressBtn}>Click</button>
        </div>
    )
}

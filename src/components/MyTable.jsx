import React, { Component } from "react";
import { Table, Button } from "react-bootstrap";

export default class MyTable extends Component {
    constructor(props){
      super(props)
      this.state = {
        data: props.data
      }
    }

    onDelete = (e) =>{
      let temp = this.state.data.filter(item=>{
        return item.id != e.target.name
      })
      this.setState({
        data: temp
      })
    }

  render() {
    console.log("Table:", this.state.data);
    let p = this.state.data.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.quote}</td>
          <td><img width="200px" src={item.image} /></td>
          <td>
            <Button name={item.id} onClick={this.onDelete} variant="danger">Delete</Button>
          </td>
        </tr>
      );
    });

    return (
      <div className="w-50 text-center m-lg-auto pt-5">
        <Table striped bordered hover variant="dark" size="sm">
          <thead>
            <tr>
              <th>#</th>
              <th>Name</th>
              <th>Quote</th>
              <th>Profile</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>{p}</tbody>
        </Table>
      </div>
    );
  }
}

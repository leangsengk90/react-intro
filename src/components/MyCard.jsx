import React, { Component } from "react";
import { Button, Card, Container, Row, Col } from "react-bootstrap";

export default class MyCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      person: props.person,
    };
  }

  onHide = (e) => {
    console.log("E", e.target.name);
    let temp = this.state.person.filter(item =>{
      return item.id != e.target.name
    })
    this.setState({
      person: temp
    },()=>{
      console.log(this.state.person);
    });
  };

  render() {
    console.log("P:", this.state.person);
    let p = this.state.person.map((item, index) => {
      return (
        <Col sm="6" md="4" lg="2" key={index}>
          <Card style={{ width: "18rem" }}>
            <Card.Img variant="top" src={item.image} />
            <Card.Body>
              <Card.Title>{item.name}</Card.Title>
              <Card.Text>{item.quote}</Card.Text>
              <Button name={item.id} variant="warning" onClick={this.onHide}>
                Hide
              </Button>
            </Card.Body>
          </Card>
        </Col>
      );
    });

    return (
      <Container fluid>
        <Row className="justify-content-lg-around mt-lg-5">{p}</Row>
      </Container>
    );
  }
}

import React, { Component } from "react";
import { Form, Button, Image } from "react-bootstrap";

export default class MyUpdatedForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      name: "Your name",
      chosenFile: null,
      fileName: null,
      imageURL: null,
      imagePlaceHolder:
        "http://isgpp.com.ng/wp-content/uploads/2014/09/isgpp_avatar_placeholder.png",
      usernameSMS: "",
      emailSMS: "",
      passwordSMS: "",
      country: "",
      gender: "male",
      isSingle: false,

      defaultFruitList: ["apple", "banana", "potato", "jerry", "melon"],
      newPerson: {
        image:
          "https://pbs.twimg.com/profile_images/988775660163252226/XpgonN0X.jpg",
        name: "Kao Leangseng",
        username: "seng123",
        email: "leangsengk90@gmail.com",
        password: "",
        country: "Vietnam",
        gender: "female",
        isSingle: true,
        fruitList: ["apple", "potato", "jerry"],
      },
    };
  }

  onUploadFile = (e) => {
    console.log("File: ", e);
    this.setState({
      chosenFile: e.target.files[0],
      fileName: e.target.files[0].name,
      imageURL: URL.createObjectURL(e.target.files[0]),
    });
  };

  //   onChangeName = (e) => {
  //     console.log("Name: ", e.key)
  //   }

  //************ Handle Text */
  onHandleText = (e) => {
    this.setState(
      (curr) => ({
        [e.target.name]: e.target.value,
      }),
      () => {
        console.log("Username: ", this.state.username);
        //*********** Username Regex */
        if (e.target.name === "username") {
          let pattern = /^[a-z0-9]{4,8}$/g;
          let resultUsername = pattern.test(this.state.username.trim());

          if (resultUsername) {
            //alert("\w 4 to 8 digits");
            this.setState({
              usernameSMS: "",
            });
          } else if (this.state.username === "") {
            this.setState({
              usernameSMS: "Username cannot be empty.",
            });
          } else {
            //alert("False");
            this.setState({
              usernameSMS:
                "Username must be lowercase letters, numbers at least 4 to 8 digits.",
            });
          }
        }

        //*********** Email Regex */
        if (e.target.name === "email") {
          let pattern1 = /^\S+@\S+\.[a-z]{3}$/g;
          let resultEmail = pattern1.test(this.state.email.trim());

          if (resultEmail) {
            this.setState({
              emailSMS: "",
            });
          } else if (this.state.email === "") {
            this.setState({
              emailSMS: "Email cannot be empty.",
            });
          } else {
            //alert("False");
            this.setState({
              emailSMS: "Email must be valid.",
            });
          }
        }

        //*********** Password Regex */
        if (e.target.name === "password") {
          let pattern2 = /^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[_\W])([a-zA-Z0-9_\W]{8,})$/g;
          let resultPassword = pattern2.test(this.state.password);

          if (resultPassword) {
            this.setState({
              passwordSMS: "",
            });
          } else if (this.state.password === "") {
            this.setState({
              passwordSMS: "Password cannot be empty.",
            });
          } else {
            //alert("False");
            this.setState({
              passwordSMS:
                "Password must be at least a capital letter, a lowercase letter, a number and special charactor with at least 8 digits.",
            });
          }
        }
      }
    );
  };

  //********** Save */
  onSave = () => {
    if (
      !this.state.usernameSMS &&
      !this.state.emailSMS &&
      !this.state.passwordSMS
    ) {
      let pattern = /^\S+$/g;
      let resultUsername = pattern.test(this.state.username.trim());
      if (resultUsername) {
        this.setState({
          usernameSMS: "",
        });
      } else {
        //alert("False");
        this.setState({
          usernameSMS: "Username cannot be empty.",
        });
      }

      let pattern2 = /^\S+$/g;
      let resultEmail = pattern2.test(this.state.email.trim());
      if (resultEmail) {
        this.setState({
          emailSMS: "",
        });
      } else {
        //alert("False");
        this.setState({
          emailSMS: "Email cannot be empty.",
        });
      }

      //let pattern3 = /^[]$/g;
      //let resultPassword = pattern3.test(this.state.password);
      if (this.state.password !== "") {
        this.setState({
          passwordSMS: "",
        });
      } else {
        //alert("False");
        this.setState({
          passwordSMS: "Password cannot be empty.",
        });
      }

      //*********** Add Data to New Person */
      this.setState(
        {
          newPerson: {
            image: this.state.chosenFile,
            name: document.getElementById("person-name").innerHTML,
            username: this.state.username,
            email: this.state.email,
            password: this.state.password,
            country: this.state.country,
            gender: this.state.gender,
            isSingle: this.state.isSingle,
            fruitList: this.state.fruitList,
          },
        },
        () => {
          console.log("New Person: ", this.state.newPerson);
        }
      );
    } else {
      //alert("Try again!")
    }
  };

  onCountry = (e) => {
    console.log(e.target.value);
    this.setState({
      country: e.target.value,
    });
  };

  onGender = (e) => {
    console.log(e.target.value);
    this.setState({
      gender: e.target.value,
    });
  };

  onSingle = (e) => {
    console.log(e.target.checked);
    this.setState({
      isSingle: e.target.checked,
    });
  };

  onFavouriteFruite = (e) => {
    console.log(e.target.checked, e.target.value);
    let temp = [e.target.value].toString().toLowerCase();
    this.setState(
      (curr) => ({
        fruitList: {
          ...curr.fruitList,
          [temp]: e.target.checked,
        },
      }),
      () => {
        //console.log("STATE: ", this.state);
      }
    );
  };

  render() {
    let list = this.state.defaultFruitList.map((item) => {
        return(
            <Form.Check
          type="checkbox"
          defaultChecked={this.state.newPerson.fruitList.some(i=>{
              return item === i
          })}
          value={item}
          id={item}
          label={item.toUpperCase()}
        />
        )
      })

    return (
      <div>
        <Form className="w-25 m-lg-auto">
          <div className="text-lg-center">
            <input
              onChange={this.onUploadFile}
              style={{ display: "none" }}
              id="upload-file"
              type="file"
            />
            <label htmlFor="upload-file">
              <Image
                className="w-50 h-50 img-thumbnail"
                style={{ minWidth: "200px", minHeight: "200px" }}
                src={
                  this.state.newPerson.image
                    ? this.state.newPerson.image
                    : this.state.imagePlaceHolder
                }
                roundedCircle
              />
            </label>
          </div>
          <h1
            id="person-name"
            suppressContentEditableWarning={true}
            contentEditable={true}
            className="text-lg-center mt-lg-2"
            // onKeyDown={this.onChangeName}
          >
            {this.state.newPerson.name}
          </h1>
          <br />
          <Form.Group>
            {/* <Form.Label>Username</Form.Label> */}
            <Form.Control
              name="username"
              value={this.state.newPerson.username}
              type="text"
              placeholder="Username"
              onChange={this.onHandleText}
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.usernameSMS}</p>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            {/* <Form.Label>Email address</Form.Label> */}
            <Form.Control
              name="email"
              value={this.state.newPerson.email}
              type="text"
              placeholder="Email"
              onChange={this.onHandleText}
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.emailSMS}</p>
            </Form.Text>
          </Form.Group>

          <Form.Group>
            {/* <Form.Label>Email address</Form.Label> */}
            <Form.Control
              name="password"
              value={this.state.newPerson.password}
              type="password"
              placeholder="Password"
              onChange={this.onHandleText}
            />
            <Form.Text className="text-muted">
              <p style={{ color: "red" }}>{this.state.passwordSMS}</p>
            </Form.Text>
          </Form.Group>

          <label htmlFor="country">Country</label>
          <Form.Control
            id="country"
            value={this.state.newPerson.country}
            onChange={this.onCountry}
            as="select"
          >
            <option>Cambodia</option>
            <option>Thailand</option>
            <option>Vietnam</option>
            <option>Laos</option>
          </Form.Control>
          <br />
          <label htmlFor="gender">Gender</label>
          <Form.Check
            label="Male"
            type="radio"
            name="gender"
            id="male"
            aria-label="radio 1"
            value="male"
            defaultChecked={
              this.state.newPerson.gender === "male" ? true : false
            }
            onChange={this.onGender}
          />

          <Form.Check
            label="Female"
            type="radio"
            name="gender"
            id="female"
            value="female"
            aria-label="radio 1"
            onChange={this.onGender}
            defaultChecked={
              this.state.newPerson.gender === "female" ? true : false
            }
          />
          <br />
          <label htmlFor="custom-switch">Status</label>
          <Form.Check
            type="switch"
            id="custom-switch"
            onChange={this.onSingle}
            label="Single"
            // value="on"
            defaultChecked={this.state.newPerson.isSingle}
          />

          <br />
          <label>Favourite Fruit</label>
          <Form.Group
            onChange={this.onFavouriteFruite}
            controlId="formBasicCheckbox"
          >
         {list}
          </Form.Group>

          {/* <Form.File id="formcheck-api-custom" custom>
            <Form.File.Input isValid />
            <Form.File.Label data-browse="Upload">
              Custom file input
            </Form.File.Label>
            <Form.Control.Feedback type="valid">
              You did it!
            </Form.Control.Feedback>
          </Form.File> */}

          <div className="m-lg-3 text-lg-center">
            <Button
              onClick={this.onSave}
              variant="success"
              type="button"
              className="w-50"
            >
              Save
            </Button>
          </div>
        </Form>
      </div>
    );
  }
}

import React from "react";
import {Card, Button} from 'react-bootstrap'

export default class Toggle extends React.Component {
   constructor(props){
       super(props)

       this.state = {
           isShow: true,
            
       }
   }

    onToggle = () => {
       this.setState({
           isShow: !this.state.isShow,
           
       })
    }

    render(){
        return (
            <div className="w-25 m-lg-auto pt-lg-5">
             {/* {this.state.isShow && ( */}
                <Card style={{visibility: this.state.isShow ? "visible" : "hidden"}}>
                <Card.Img variant="top" src="https://imgd.aeplcdn.com/476x268/n/cw/ec/38904/mt-15-front-view.jpeg?q=80" />
                <Card.Body>
                  <Card.Title>Card Title</Card.Title>
                  <Card.Text>
                    Some quick example text to build on the card title and make up the
                    bulk of the card's content.
                  </Card.Text>
                  {/* <Button variant="primary">Go somewhere</Button> */}
                </Card.Body>
              </Card>
             {/* )} */}
              <Button onClick={this.onToggle} variant="success" className="w-100 my-lg-4">{this.state.isShow?"Hide":"Show"}</Button>
            </div>
          );
    }
}

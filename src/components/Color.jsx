import React from 'react'

const color = "RED"

function NewColor(props){
    return props.test
}

export let changeColor = () => {
    return "CHANGE COLOR"
}

class FavouriteColor extends React.Component{
    constructor(props){
        super(props)

        this.abc = {
            name: "name"
        }
    }

    test(){
        this.setState({
            name: "data",
        },()=>{
            console.log(this.abc.name);
        })
    }


    render(){
        console.log("Class test");
        return(
            <div>
                {/* <h1 id="my-header">Favourite Color</h1> */}
                {/* <h1 style={myHeader}>{this.props.username}</h1> */}
                <h1 style={{color: "orange", fontSize:"50px"}}>{this.props.username}</h1>
            </div>
        )
    }
}

FavouriteColor.defaultProps = {
    username: "tatatata"
}


export default FavouriteColor;
export {color, NewColor}

//************* STYLE */

const myHeader = {
    color: "blue",
    fontSize: "100px",
    border: "2px solid green"
}